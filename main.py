# Напишите пример содержащий функцию принимающую *args
def razdelitel():
    print("--------------------------------")
def sum_10(*args):
    result = 0
    for i in args:
        result = i+10
        return result
res = sum_10(23)
print(res)
razdelitel()
# Напишите пример содержащий функцию принимающую *kwargs
def kwargs(**kwargs):
    print('----kwargs---',kwargs)
kwargs(v=2,b=3,n=4)
razdelitel()
# Напишите пример содержащий функцию принимающую *args и *kwargs
def argkwarg(*args,**kwargs):
    print('---args---',args)
    print('---kwargs---',kwargs)
argkwarg(1, 3, 5, c=2, s=32, z=12)
razdelitel()
# Напишите пример содержащий функцию принимающую *args и *kwargs а также простые агрументы
def arg_kwarg_prost(u,i,o,*args,**kwargs):
    print('---argumenti---',u,i,o)
    print('---args---',args)
    print('---kwargs---',kwargs)
arg_kwarg_prost(1,2,3,4,5,6, l=2, r=5)
razdelitel()
# Напишите пример lambda функции которая возвращает результат умножения числа пришедшего в аргументы на 5
mult5 = lambda num: num*5
re = mult5(321)
print(re)
razdelitel()
# Напишите пример lambda функции которая возвращает результат деления числа пришедшего в аргументы на 5
del5 = lambda num2: num2/5
re2 = del5(555)
print(re2)
razdelitel()
# Напишите пример lambda функции которая возвращает значение 'age' из пришедшего в аргументы словаря
friend = {
    'name': 'Igor',
    'age': 18,
    'gender': 'male'
}
z = lambda friend: friend['age']
print(z)
razdelitel()
# Напишите пример создания list
my_list = [1,2,3,4,5]
print(my_list)
# Напишите чтения из list

# Напишите пример изменения list

# Напишите пример добавления в list
my_list.append(6)
print(my_list)
razdelitel()
# Напишите пример удаления из list
my_list.pop(2)
print(my_list)
razdelitel()
# Напишите пример перевода list в tuple
my_list = tuple(my_list)
print(my_list)
razdelitel()
# Напишите пример создания tuple
my_tuple = (1,2,3,4,5,6)
print(my_tuple)
razdelitel()
# Напишите чтения из tuple

# Напишите пример перевода tuple в list
my_tuple = list(my_tuple)
print(my_tuple)
razdelitel()
# Напишите пример создания dict
my_dict = {
    'name': 'Igor',
    'age': 19,
    'gender':'male'
}
# Напишите пример чтения из dict

# Напишите пример изменения dict
my_dict['name'] = 'Vlad'
my_dict['age'] = 23
print(my_dict)
# Напишите пример добавления в dict
my_dict.update()
# Напишите пример удаления из dict

# Напишите пример применения lambda функции в методе list (sort)

# Почитате про python sorted

# Напишите пример применения lambda функции в методе list (sorted)

# Напишите пример создания set
